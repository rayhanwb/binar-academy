class nilaiSiswa {
  nilai;

  constructor(nilai) {
    this.nilai = nilai;
  }

  sorted() {
    return this.nilai.sort(function (a, b) {
      return a - b;
    });
  }

  average() {
    let result = this.nilai.map(function (x) {
      return parseInt(x, 10);
    });
    let sum = result.reduce((partialSum, a) => partialSum + a, 0);
    return sum / this.nilai.length;
  }

  lowest() {
    return this.sorted()[0];
  }

  highest() {
    return this.sorted()[this.sorted().length - 1];
  }

  pass() {
    let passed = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] >= 75) {
        passed += 1;
      }
    }
    return passed;
  }
  fail() {
    let failed = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] < 75) {
        failed += 1;
      }
    }
    return failed;
  }
}

var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var arrNilai = [];

console.log('Input Nilai Siswa:');
console.log('*Untuk mengakhiri inputan isikan dengan -> q \n');

rl.on('line', (nilai) => {
  if (nilai != 'q') {
    arrNilai.push(nilai);
  } else {
    rl.close();
  }
});
rl.on('close', () => {
  const nilaiKelasTiga = new nilaiSiswa(arrNilai);
  console.log('\nNilai rata - rata : ' + nilaiKelasTiga.average());
  console.log('\nNilai tertinggi : ' + nilaiKelasTiga.highest());
  console.log('\nNilai terendah : ' + nilaiKelasTiga.lowest());
  console.log('\nJumlah siswa lulus : ' + nilaiKelasTiga.pass());
  console.log('\nJumlah siswa tidak lulus : ' + nilaiKelasTiga.fail());
  console.log('\nUrutan Nilai : ' + nilaiKelasTiga.sorted());
});
