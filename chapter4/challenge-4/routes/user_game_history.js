const express = require('express');
const router = express.Router();

const {
  createUserHistory,
  getUserHistories,
  getUserHistory,
  updateUserHistory,
  deleteUserHistory
} = require('../controller/user_game_history');

router.post('/', createUserHistory);
router.get('/', getUserHistories);
router.get('/:id', getUserHistory);
router.put('/:id', updateUserHistory);
router.delete('/:id', deleteUserHistory);

module.exports = router;
