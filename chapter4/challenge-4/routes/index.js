const express = require('express');
const router = express.Router();

const user_game = require('./user_game');
const user_game_biodata = require('./user_game_biodata');
const user_game_history = require('./user_game_history');

router.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: 'Welcome to Game API',
    data: null
  });
});

router.use('/user', user_game);
router.use('/biodata', user_game_biodata);
router.use('/history', user_game_history);
module.exports = router;
