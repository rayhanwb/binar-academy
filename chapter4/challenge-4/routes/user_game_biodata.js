const express = require('express');
const router = express.Router();

const {
  createUserBiodata,
  getUserBiodatas,
  getUserBiodata,
  updateUserBiodata,
  deleteUserBiodata
} = require('../controller/user_game_biodata');

router.post('/', createUserBiodata);
router.get('/', getUserBiodatas);
router.get('/:id', getUserBiodata);
router.put('/:id', updateUserBiodata);
router.delete('/:id', deleteUserBiodata);

module.exports = router;
