const express = require('express');
const morgan = require('morgan');
const app = express();
const port = 3030;
const router = require('./routes');

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/', router);

app.listen(port, () => {
  console.log('running on port', port);
});
