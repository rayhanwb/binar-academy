const { user_game_history } = require('../models');

createUserHistory = async (req, res) => {
  try {
    let {
      user_id,
      game_title,
      game_level,
      score,
      session_start,
      session_stop
    } = req.body;

    let newUser = await user_game_history.create({
      user_id,
      game_title,
      game_level,
      score,
      session_start,
      session_stop
    });

    res.status(201).json({
      status: 'success',
      message: 'succesfully create user game history',
      data: newUser
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      errors: err
    });
  }
};

getUserHistories = async (req, res) => {
  try {
    let users = await user_game_history.findAll({ include: 'owner' });

    res.status(200).json({
      status: 'success',
      message: 'Succesfully get all user game history',
      data: users
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      errors: err
    });
  }
};

getUserHistory = async (req, res) => {
  try {
    const user_id = req.params.id;

    let user = await user_game_history.findOne({
      include: 'owner',
      where: {
        id: user_id
      }
    });

    if (!user) {
      res.status(404).json({
        status: 'error',
        message: 'Cant find user game history with id ' + user_id,
        data: null
      });
      return;
    }

    res.status(200).json({
      status: 'success',
      message: 'Succesfully get user game history with id ' + user_id,
      data: user
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      errors: err
    });
  }
};

updateUserHistory = async (req, res) => {
  try {
    const id_user = req.params.id;
    const {
      user_id,
      game_title,
      game_level,
      score,
      session_start,
      session_stop
    } = req.body;

    let query = {
      where: {
        id: id_user
      }
    };

    let updated = await user_game_history.update(
      {
        user_id,
        game_title,
        game_level,
        score,
        session_start,
        session_stop
      },
      query
    );

    res.status(200).json({
      status: 'success',
      message: 'Succesfully update user game history',
      data: updated
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      errors: err
    });
  }
};

deleteUserHistory = async (req, res) => {
  try {
    const id_user = req.params.id;

    let deleted = await user_game_history.destroy({
      where: {
        id: id_user
      }
    });

    res.status(200).json({
      status: 'success',
      message: 'Succesfully delete user game history',
      data: deleted
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 'error',
      errors: err
    });
  }
};

module.exports = {
  createUserHistory,
  getUserHistories,
  getUserHistory,
  updateUserHistory,
  deleteUserHistory
};
