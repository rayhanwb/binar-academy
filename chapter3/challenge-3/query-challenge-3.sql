-- Membuat database
CREATE DATABASE game_data;

-- Membuat tabel
CREATE TABLE user_game(id BIGSERIAL PRIMARY KEY, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL);

CREATE TABLE user_game_biodata( id BIGSERIAL PRIMARY KEY, user_id BIGINT, email VARCHAR(255), nama_lengkap VARCHAR(255), telpon VARCHAR(255), CONSTRAINT user_id FOREIGN KEY(user_id) REFERENCES user_game(id));

CREATE TABLE user_game_history( id BIGSERIAL PRIMARY KEY, user_id BIGINT, game_title VARCHAR(255), game_level VARCHAR(255),score BIGINT, session_start timestamp, session_stop timestamp, CONSTRAINT user_id FOREIGN KEY(user_id) REFERENCES user_game(id));

-- Mengisi tabel

INSERT INTO user_game (username,password) VALUES ('Anthon1','123456'),('James21','Password'),('s1mple','abc123'),('aqbz','qwerty'),('ses4me','sesameseed'),('tonijak','antonitoni');

INSERT INTO user_game_biodata (user_id,email,nama_lengkap,telpon) VALUES ('1','Anthon1@mail.com','Anthoni Salim','08123124'),('2','James21@mail.com','James Riady','081268587'),('3','s1mple@mail.com','Gerard Rose','08126465877'),('4','aqbz@mail.com','Asep Sultan','0812781374'),('5','ses4me@mail.com','Wijin Andreas','0812093205');

INSERT INTO user_game_history (user_id, game_title, game_level,score,session_start, session_stop) VALUES ('1','Fight Punch','1',200,'2022-03-03 04:30:26','2022-03-03 04:55:12'),('1','Fight Punch','2',270,'2022-03-03 05:00:26','2022-03-03 05:35:12'),('2','Pond Fishing','15',780,'2022-03-03 06:00:26','2022-03-03 07:35:12'),('4','Pond Fishing','15',780,'2022-03-03 06:00:26','2022-03-03 07:35:12'),('4','It Takes Two','15',780,'2022-03-03 06:36:26','2022-03-03 07:35:12'),('5','It Takes Two','15',780,'2022-03-03 06:36:26','2022-03-03 07:35:12');

-- Melihat isi tabel

SELECT * FROM user_game;
SELECT * FROM user_game_biodata;
SELECT * FROM user_game_history;

-- Melihat gabungan user_game dan user_game_biodata
SELECT * FROM user_game JOIN user_game_biodata ON user_game_biodata.user_id = user_game.id;

-- Melihat user dan gamenya
SELECT * FROM user_game JOIN user_game_history  ON user_game_history.user_id = user_game.id;

-- Melihat user, biodata dan gamenya
SELECT username, user_game_history.user_id,user_game_history.game_title, user_game_history.game_level,user_game_history.score,user_game_history.session_start,user_game_history.session_stop, user_game_biodata.email, user_game_biodata.nama_lengkap, user_game_biodata.telpon FROM user_game 
JOIN user_game_history  ON user_game_history.user_id = user_game.id
JOIN user_game_biodata  ON user_game_biodata.user_id = user_game.id;

SELECT username, user_game_history.user_id,user_game_history.game_title, user_game_history.game_level,user_game_history.score,user_game_history.session_start,user_game_history.session_stop, user_game_biodata.email, user_game_biodata.nama_lengkap, user_game_biodata.telpon FROM user_game 
FULL JOIN user_game_history  ON user_game_history.user_id = user_game.id
FULL JOIN user_game_biodata  ON user_game_biodata.user_id = user_game.id;

-- Mengubah data pada tabel
UPDATE user_game
SET username  ='simple'
WHERE username='s1mple';

-- Menghapus data pada tabel
DELETE FROM user_game
WHERE username='tonijak';
