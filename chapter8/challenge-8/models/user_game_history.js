'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_history.belongsTo(models.user_game, {
        foreignKey: 'user_id',
        as: 'owner'
      });
    }
  }
  user_game_history.init(
    {
      user_id: DataTypes.INTEGER,
      game_title: DataTypes.STRING,
      game_level: DataTypes.STRING,
      score: DataTypes.INTEGER,
      session_start: DataTypes.DATE,
      session_stop: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'user_game_history'
    }
  );
  return user_game_history;
};
