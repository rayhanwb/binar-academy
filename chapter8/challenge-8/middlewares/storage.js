const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads');
    },

    filename: function (req, file, callback) {
        const fileName = Date.now() + '-' + path.extname(file.originalname);
        callback(null, fileName);
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, callback) => {
        if (file.mimetype == 'video/mp4' || file.mimetype == 'video/mkv' || file.mimetype == 'video/mov') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('Cant upload file extension other than mp4, mkv, and mov'));
        }
    },
    onError: function (err, next) {
        console.log('error', err);
        next(err);
    }
});

module.exports = upload;