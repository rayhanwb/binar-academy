const { user_game } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const helper = require("../helper");

const { JWT_PRIVATE_KEY } = process.env;

module.exports = {
  register: async (req, res) => {
    try {
      const { username, email, password } = req.body;
      const isExist = await user_game.findOne({
        where: { email: email },
      });

      if (isExist) {
        return res.status(400).json({
          status: false,
          message: "User already exist",
          data: null,
        });
      }

      const hashPassword = await bcrypt.hash(password, 10);

      const newuser_game = await user_game.create({
        username: username,
        email: email,
        password: hashPassword,
      });

      res.status(201).json({
        status: true,
        message: "User registered",
        data: {
          id: newuser_game.id,
          username: newuser_game.username,
          email: newuser_game.email,
          updatedAt: newuser_game.updatedAt,
          createdAt: newuser_game.createdAt,
        },
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  login: async (req, res) => {
    try {
      const user = await user_game.authenticate(req.body);
      const accesstoken = user.generateToken();

      res.status(200).json({
        status: true,
        message: "Login success",
        data: {
          id: user.id,
          username: user.username,
          access_token: accesstoken,
          created_at: user.createdAt,
          updated_at: user.updatedAt,
        },
      });
    } catch (err) {
      res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  forgot: async (req, res) => {
    try {
      const { email } = req.body;

      const user = await user_game.findOne({ where: { email } });
      if (user) {
        const data = {
          id: user.id,
          username: user.username,
          email: user.email,
        };

        const token = await jwt.sign(data, JWT_PRIVATE_KEY);

        const link = `${req.protocol}://${req.get(
          "host"
        )}/api/reset-password?token=${token}`;

        const html = `
              <p>Hi ${user.name}, Klik <a href="${link}" target="_blank">link ini</a> untuk melakukan reset password.</p>
              `;

        helper.sendEmail(user.email, "Reset Password", html);
      }

      return res.json({
        status: true,
        message: "Metode reset password sudah dikirim ke email anda.",
        data: null,
      });
    } catch (err) {
      return res.status(500).json({
        status: true,
        message: err.message,
        data: null,
      });
    }
  },

  reset: async (req, res) => {
    try {
      const token = req.query.token;
      const { new_password, confirm_new_password } = req.body;

      if (new_password != confirm_new_password) {
        return res.status(400).json({
          status: false,
          message: "Password harus sesuai.",
          data: null,
        });
      }

      const data = await jwt.verify(token, JWT_PRIVATE_KEY);

      const encryptedPassword = await bcrypt.hash(new_password, 10);

      let query = {
        where: {
          email: data.email,
        },
      };

      let updated = await User.update(
        {
          password: encryptedPassword,
        },
        query
      );

      return res.json({
        status: true,
        message: "Password berhasil diganti",
        data: updated,
      });
    } catch (err) {
      return res.status(500).json({
        status: true,
        message: err.message,
        data: null,
      });
    }
  },
};
