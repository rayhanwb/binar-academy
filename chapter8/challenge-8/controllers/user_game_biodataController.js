const { user_game_biodata } = require('../models');

module.exports = {
  createUserBiodata: async (req, res) => {
    try {
      let { user_id, email, nama_lengkap, telpon } = req.body;

      let newUser = await user_game_biodata.create({
        user_id,
        email,
        nama_lengkap,
        telpon
      });

      res.status(201).json({
        status: 'success',
        message: 'succesfully create user biodata',
        data: newUser
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  getUserBiodatas: async (req, res) => {
    try {
      let users = await user_game_biodata.findAll({ include: 'owner' });

      res.status(200).json({
        status: 'success',
        message: 'Succesfully get all user biodata',
        data: users
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  getUserBiodata: async (req, res) => {
    try {
      const user_id = req.params.id;

      let user = await user_game_biodata.findOne({
        include: 'owner',
        where: {
          id: user_id
        }
      });

      if (!user) {
        res.status(404).json({
          status: 'error',
          message: 'Cant find user biodata with id ' + user_id,
          data: null
        });
        return;
      }

      res.status(200).json({
        status: 'success',
        message: 'Succesfully get user biodata with id ' + user_id,
        data: user
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  updateUserBiodata: async (req, res) => {
    try {
      const id_user = req.params.id;
      const { user_id, email, nama_lengkap, telpon } = req.body;

      let query = {
        where: {
          id: id_user
        }
      };

      let updated = await user_game_biodata.update(
        {
          user_id,
          email,
          nama_lengkap,
          telpon
        },
        query
      );

      res.status(200).json({
        status: 'success',
        message: 'Succesfully update user biodata',
        data: updated
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  deleteUserBiodata: async (req, res) => {
    try {
      const id_user = req.params.id;

      let deleted = await user_game_biodata.destroy({
        where: {
          id: id_user
        }
      });

      res.status(200).json({
        status: 'success',
        message: 'Succesfully delete user biodata',
        data: deleted
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  }
};
