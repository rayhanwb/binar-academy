# Challenge Chapter 7

# Alur Penggunaan
1. Konfigurasi .env
2. ```bash
   npm install
   sequelize db:create
   sequelize db:migrate
   node app.js
   ```
3. Registrasi melalui http://localhost:3000/api/auth/register/ dengan format
    ```JSON
    {
        "username":"xxxxxxx",
        "password":"xxxxxxx"
    }
    ```

4. Login melalui http://localhost:3000/api/auth/login/ dengan format
    ```JSON
    {
        "username":"xxxxxxx",
        "password":"xxxxxxx"
    }
    ```
    untuk mendapatkan `access_token`

5. Gunakan `access_token` untuk mengisi Authorization pada Headers untuk mengakses endpoint api lainnya -> http://localhost:3000/api-docs/

6. Untuk melakukan upload video gunakan endpoint -> http://localhost:3000/api/upload/