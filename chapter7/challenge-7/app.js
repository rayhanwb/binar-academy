require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const app = express();

const router = require('./router');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const PORT = process.env.PORT || 3000;

app.use("/video", express.static("public/uploads"));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.set('view engine', 'ejs');
app.use(express.json());
app.use(morgan('dev'));
app.use('/api',router);


app.listen(PORT, () => console.log('listening on port', PORT));

