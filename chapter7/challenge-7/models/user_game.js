'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }


  checkPassword = password => {
    return bcrypt.compareSync(password, this.password);
  };

  generateToken = () => {
    const payload = {
      id: this.id,
      username: this.username,
    };

    const secretKey = process.env.JWT_SECRET_KEY;

    const token = jwt.sign(payload, secretKey);
    return token;
  };

  static authenticate = async ({ username, password }) => {
    try {
      const user = await this.findOne({
        where: {
          username: username
        }
      });
      if (!user) return Promise.reject(new Error('user not found!'));

      const isPasswordValid = user.checkPassword(password);
      if (!isPasswordValid) return Promise.reject(new Error('wrong password!'));

      return Promise.resolve(user);

    } catch (err) {
      return Promise.reject(err);
    }
  };
}
  user_game.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};

