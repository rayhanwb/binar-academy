
const { Video } = require('../models');

module.exports = {
    single: async (req, res) => {
        try {
            const videoUrl = req.protocol + '://' + req.get('host') + '/video/' + req.file.filename;

            const video = await Video.create({
                title: req.file.filename,
                url: videoUrl
            });

            res.status(200).json({
                status: true,
                message: "Upload success",
                data: video
            });

        } catch (err) {
            return res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
};