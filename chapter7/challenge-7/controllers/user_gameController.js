const { user_game } = require('../models');

module.exports = {
  
  createUser: async (req, res) => {
    try {
      let { username, password } = req.body;

      let newUser = await user_game.create({
        username,
        password
      });

      res.status(201).json({
        status: 'success',
        message: 'Succesfully create user',
        data: newUser
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  getUsers: async (req, res) => {
    try {
      let users = await user_game.findAll();

      res.status(200).json({
        status: 'success',
        message: 'Succesfully get all user',
        data: users
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  getUser: async (req, res) => {
    try {
      const user_id = req.params.id;

      const user = await user_game.findOne({ where: { id: user_id } });

      if (!user) {
        res.status(404).json({
          status: 'error',
          message: 'Cant find user with id ' + user_id,
          data: null
        });
        return;
      }

      res.status(200).json({
        status: 'success',
        message: 'Succesfully get user with id ' + user_id,
        data: user
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  updateUser: async (req, res) => {
    try {
      const user_id = req.params.id;
      const { username, password } = req.body;

      let query = {
        where: {
          id: user_id
        }
      };

      let updated = await user_game.update(
        {
          username,
          password
        },
        query
      );

      res.status(200).json({
        status: 'success',
        message: 'Succesfully update user',
        data: updated
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  },

  deleteUser: async (req, res) => {
    try {
      const user_id = req.params.id;

      let deleted = await user_game.destroy({
        where: {
          id: user_id
        }
      });

      res.status(200).json({
        status: 'success',
        message: 'Succesfully delete user',
        data: deleted
      });
    } catch (err) {
      console.log(err);
      res.status(404).json({
        status: 'error',
        errors: err
      });
    }
  }
};
