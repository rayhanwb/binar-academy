const { user_game } = require('../models');
const bcrypt = require('bcrypt');

module.exports = {
    register: async (req, res) => {
        try {
            const { username, password } = req.body;
            const isExist = await user_game.findOne({ where: { username: username } });

            if (isExist) {
                return res.status(400).json({
                    status: false,
                    message: 'User already exist',
                    data: null
                });
            }

            const hashPassword = await bcrypt.hash(password, 10);

            const newuser_game = await user_game.create({
                username: username,
                password: hashPassword
            });

            res.status(201).json({
                status: true,
                message: 'User registered',
                data: {
                    id: newuser_game.id,
                    username: newuser_game.username,
                    updatedAt: newuser_game.updatedAt,
                    createdAt: newuser_game.createdAt
                }
            });

        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    },

    login: async (req, res) => {
        try {
            const user = await user_game.authenticate(req.body);
            const accesstoken = user.generateToken();

            res.status(200).json({
                status: true,
                message: 'Login success',
                data: {
                    id: user.id,
                    username: user.username,
                    access_token: accesstoken,
                    created_at: user.createdAt,
                    updated_at: user.updatedAt,
                }
            });
        } catch (err) {
            res.status(500).json({
                status: false,
                message: err.message,
                data: null
            });
        }
    }
};