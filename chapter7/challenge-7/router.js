const express = require('express');
const router = express.Router();

const user = require('./controllers/user_gameController');
const biodata = require('./controllers/user_game_biodataController');
const history = require('./controllers/user_game_historyController');
const frontend = require('./controllers/frontendController');
const auth = require('./controllers/AuthController');
const middleware = require('./middlewares');

const storage = require('./middlewares/storage');
const media = require('./controllers/mediaController');

router.post('/auth/register', auth.register);
router.post('/auth/login', auth.login);

router.get('/users', middleware.login, user.getUsers);
router.get('/users/:id', middleware.login, user.getUser);
router.post('/users', middleware.login, user.createUser);
router.put('/users/:id', middleware.login, user.updateUser);
router.delete('/users/:id', middleware.login, user.deleteUser);

router.get('/biodata', middleware.login, biodata.getUserBiodatas);
router.get('/biodata/:id', middleware.login, biodata.getUserBiodata);
router.post('/biodata', middleware.login, biodata.createUserBiodata);
router.put('/biodata/:id', middleware.login, biodata.updateUserBiodata);
router.delete('/biodata/:id', middleware.login, biodata.deleteUserBiodata);

router.get('/history', middleware.login, history.getUserHistories);
router.get('/history/:id', middleware.login, history.getUserHistory);
router.post('/history', middleware.login, history.createUserHistory);
router.put('/history/:id', middleware.login, history.updateUserHistory);
router.delete('/history/:id', middleware.login, history.deleteUserHistory);

router.post('/upload/', middleware.login, storage.single('video'), media.single);

module.exports = router;
