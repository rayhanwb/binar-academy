const { Usergamehistory, Usergame } = require('../models')

createHistory = async (req, res) => {
    try {
        let { user_id, score } = req.body

        let UserName = await Usergame.findOne({ where: { id: user_id } });
        if (!UserName) return res.respondNotFound(`User with ${user_id} not found`)

        let newUser = await Usergamehistory.create({
            user_id,
            score
        })

        res.respondCreated(newUser)

    } catch (err) {
        res.respondServerError(err.message)
    }
}

getAllHistory = async (req, res) => {
    try {
        let Users = await Usergamehistory.findAll({ include: 'owner' });

        res.respondSuccess(Users)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

getHistory = async (req, res) => {
    try {
        const User_id = req.params.id

        let User = await Usergamehistory.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find history with id ${User_id}`)

        res.respondSuccess(User)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

updateHistory = async (req, res) => {
    try {
        const User_id = req.params.id
        const { user_id, score } = req.body

        let User = await Usergamehistory.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find history with id ${User_id}`)

        let query = {
            where: {
                id: User_id
            }
        }

        let updated = Usergamehistory.update({
            user_id,
            score
        }, query)

        res.respondUpdated(updated)

    } catch (err) {
        res.respondServerError(err.message)
    }
}

deleteHistory = async (req, res) => {
    try {
        const User_id = req.params.id

        let User = await Usergamehistory.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find history with id ${User_id}`)
        
        let deleted = await Usergamehistory.destroy({
            where: {
                id: User_id
            }
        })
        res.respondDeleted(deleted)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

module.exports = {
    createHistory,
    getAllHistory,
    getHistory,
    updateHistory,
    deleteHistory
}