const { Usergamebiodata, Usergame } = require('../models')

createBiodata = async (req, res) => {
    try {
        let { user_id, fullname, city } = req.body

        let UserBiodata = await Usergamebiodata.findOne({ where: { user_id: user_id } });
        if (UserBiodata) return res.respondBadRequest(`Biodata with ${user_id} is already exist`)
        
        let UserGame = await Usergamebiodata.findOne({ where: { id: user_id } });
        if (!UserGame) return res.respondNotFound(`Biodata with ${user_id} not found`)

        let newUser = await Usergamebiodata.create({
            user_id,
            fullname,
            city
        })

        res.respondCreated(newUser)

    } catch (err) {
        res.respondServerError(err.message)
    }
}

getAllBiodata = async (req, res) => {
    try {
        let Users = await Usergamebiodata.findAll({ include: 'owner' });

        res.respondSuccess(Users)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

getBiodata = async (req, res) => {
    try {
        const User_id = req.params.id

        let User = await Usergamebiodata.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find User with id ${User_id}`)

        res.respondSuccess(User)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

updateBiodata = async (req, res) => {
    try {
        const User_id = req.params.id
        const { user_id, fullname, city } = req.body

        let User = await Usergamebiodata.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find biodata with id ${User_id}`)

        let query = {
            where: {
                id: User_id
            }
        }

        let updated = Usergamebiodata.update({
            user_id,
            fullname,
            city
        }, query)

        res.respondUpdated(updated)

    } catch (err) {
        res.respondServerError(err.message)
    }
}

deleteBiodata = async (req, res) => {
    try {
        const User_id = req.params.id

        let User = await Usergamebiodata.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find biodata with id ${User_id}`)

        let deleted = await Usergamebiodata.destroy({
            where: {
                id: User_id
            }
        })
        res.respondDeleted(deleted)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

module.exports = {
    createBiodata,
    getAllBiodata,
    getBiodata,
    updateBiodata,
    deleteBiodata
}