const request = require('supertest');
const app = require('../app');
const truncate = require('../helpers/truncate');
const history = require('../controllers/usergamebiodata')

// test endpoint create history
describe('test create history endpoint', () => {
    const data = {
        user_id: 1,
        score: 100
      };
      const fectData = {
          user_id: 2,
          score: 50
      };

    test('it should return success', async () => {
      try {
        await truncate.Usergamehistory();
        const res = await request(app).post('/history').send(data);
  
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success create data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).post('/history').send(fectData);
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('User with 2 not found');
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint update history
describe('test create update endpoint', () => {
    const data = {
        user_id: 1,
        score: 100
      };
      const fectData = {
          user_id: 2,
          score: 50
      }
    test('it should return success', async () => {
      try {
        const res = await request(app).put('/history').send(data);
  
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success update data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).put('/history').send(fectData);
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find history with id 2');
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint info history
describe('test info history endpoint', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).get('/history/1');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('successfully get data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).get('/history/3');
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('history notfound');
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint get history
describe('GET /history', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).get('/history');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('successfully get data');
      } catch (err) {
        console.log(err);
      }
    });
  });

// test endpoint delete history
describe('test delete history endpoint', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).delete('/history/1');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success delete data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).get('/history/3');
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find history with id 3');
      } catch (err) {
        console.log(err);
      }
    });
  });
