const request = require("supertest");
const app = require("../app");

describe("User Game Get Test", () => {
    test('Get User', (done) => {
        request(app)
            .get('/users')
            .then(res => {
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty('status');
                expect(res.body).toHaveProperty('message');
                expect(res.body.status).toBe(true);
                expect(res.body.message).toBe('success');
                done();
            })
            .catch(err => {
                console.log(err);
                done(err);
            });
    });

});

describe('User Game Get Spesific Test', () => {
    test('Get Spesific User (Positive Test)', async () => {
      try {
        const res = await request(app).get('/users/9');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success');
      } catch (err) {
        console.log(err);
      }
    });
    test('Get Spesific User (Negative Test)', async () => {
      try {
        const res = await request(app).get('/users/3');
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body.status).toBe(false);
      } catch (err) {
        console.log(err);
      }
    });
  });


const user1 = {
    username: 'tarnoz',
    email: 'tarno@gmail.com',
    password: 'tarnoooo'
};

const user2 = {
    username: 'tarnoz',
    email: 'tarno@gmail.com',
    password: 'tarnoooo'
};

describe('User Game Post Test', () => {
    test('Create User (Positive Test)', async () => {
        try {
            const res = await request(app).post('/users').send(user1);

            expect(res.status).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success create data');
            expect(res.body.data.email).toBe(user1.email);

        } catch (err) {
            console.log({ error_message: err.message });
            expect(err).toBe('error');
        }
    });
    test('Create User (Negative Test)', async () => {
        try {
            const res = await request(app).post('/users').send(user2);

            expect(res.status).toBe(400);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe('bad request');

        } catch (err) {
            console.log({ error_message: err.message });
        }
    });
});

describe('User Game Update Test', () => {
    test('Update User (Positive Test)', async () => {
      try {
        const res = await request(app).put('/biodata').send(user1);
  
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success update data');
      } catch (err) {
        console.log({ error_message: err.message });
      }
    });
    test('Update User (Negative Test)', async () => {
      try {
        const res = await request(app).put('/biodata').send(user1);
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find user with id 1');
      } catch (err) {
        console.log({ error_message: err.message });
      }
    });
  });

describe('User Game Delete Test', () => {
    test('Delete User (Positive Test)', async () => {
      try {
        const res = await request(app).delete('/users/9');

        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success delete data');
      } catch (err) {
        console.log(err);
      }
    });
    test('Delete User (Negative Test)', async () => {
      try {
        const res = await request(app).get('/users/3');

        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find User with id 3');
      } catch (err) {
        console.log(err);
      }
    });
});