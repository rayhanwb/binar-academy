const biodata = require('../controllers/usergamebiodata')
const app = require('../app');
const request = require('supertest');
const truncate = require('../helpers/truncate');

describe('GET /biodata', () => {
    test('it should return success', async() => {
        try{
            const res = await request(app).get('/biodata')
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        }catch (err) {
            expect(err).toBe('error')
        }
    })
})
const user1 = {
    user_id: 1,
    fullname: "Joko",
    city: "Jakarta"
  };
  const user2 = {
      user_id: 8,
      fullname: "Sabrina",
        city: "Jakarta"
  };

// test endpoint create biodata
describe('test create history endpoint', () => {
    test('it should return success', async () => {
      try {
        await truncate.Usergamebiodata();
        const res = await request(app).post('/biodata').send(user1);
  
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success create data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).post('/biodata').send(user2);
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('Biodata with 1 not found');
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint update history
describe('test create update endpoint', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).put('/biodata').send(user1);
  
        expect(res.status).toBe(201);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success update data');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).put('/biodata').send(user1);
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find history with id 1');
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint info history
describe('test info history endpoint', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).get('/biodata/9');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success');
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).get('/biodata/3');
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body.status).toBe(false);
      } catch (err) {
        console.log(err);
      }
    });
  });


// test endpoint get history
describe('GET /history', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).get('/biodata');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe('success');
      } catch (err) {
        console.log(err);
      }
    });
  });

// test endpoint delete history
describe('test delete history endpoint', () => {
    test('it should return success', async () => {
      try {
        const res = await request(app).delete('/biodata/9');
  
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(true);
        expect(res.body.message).toBe(1);
      } catch (err) {
        console.log(err);
      }
    });
    test('it should return err notfound', async () => {
      try {
        const res = await request(app).get('/biodata/3');
  
        expect(res.status).toBe(404);
        expect(res.body).toHaveProperty('status');
        expect(res.body).toHaveProperty('message');
        expect(res.body.status).toBe(false);
        expect(res.body.message).toBe('cant find User with id 3');
      } catch (err) {
        console.log(err);
      }
    });
  });
