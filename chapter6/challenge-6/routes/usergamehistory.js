const express = require('express')
const router = express.Router()

const { createHistory, getAllHistory,getHistory,updateHistory,deleteHistory } = require('../controllers/usergamehistory')

router.get('/', getAllHistory)
router.get('/:id', getHistory)
router.post('/', createHistory)
router.put('/:id', updateHistory)
router.delete('/:id', deleteHistory)

module.exports = router