const express = require('express')
const router = express.Router()

const { createBiodata, getAllBiodata,getBiodata,updateBiodata,deleteBiodata } = require('../controllers/usergamebiodata')

router.get('/', getAllBiodata)
router.get('/:id', getBiodata)
router.post('/', createBiodata)
router.put('/:id', updateBiodata)
router.delete('/:id', deleteBiodata)

module.exports = router