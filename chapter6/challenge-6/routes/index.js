const express= require('express')
const router = express.Router()

const usergame = require('./usergame')
const usergamebiodata = require('./usergamebiodata')
const usergamehistory = require('./usergamehistory')

router.get('/',(req,res)=>{
    res.status(200).json({
        status:'success',
        message:'Welcome to website game',
        data:null
    })
})

router.use('/users',usergame)
router.use('/biodata',usergamebiodata)
router.use('/history',usergamehistory)

module.exports = router