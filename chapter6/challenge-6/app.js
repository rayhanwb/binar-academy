require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const app = express()
const router = require('./routes')
const helperResponse = require('./helpers/response');
const swaggerJSON = require('./swagger.json')
const swaggerUi = require('swagger-ui-express')
const { PORT } = process.env;

app.use(express.json())
app.use(morgan('dev'))
app.use(helperResponse)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerJSON))
app.use('/', router)

module.exports = app;