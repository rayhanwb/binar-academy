const username = 'admin';
const password = "admin";
const database = 'challenge-4';
const host = "127.0.0.1";
const port = "5432";
const dialect = 'postgres';

module.exports = {
  "development": {
    "username": username,
    "password": password,
    "database": database,
    "host":host,
    "port":port,
    "dialect": dialect
  },
  "test": {
    "username": username,
    "password": password,
    "database": database,
    "host": host,
    "port":port,
    "dialect": dialect
  },
  "production": {
    "username": username,
    "password": password,
    "database": database,
    "host": host,
    "port":port,
    "dialect": dialect
  }
}
