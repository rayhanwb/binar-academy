'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usergamebiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Usergamebiodata.belongsTo(models.Usergame, { foreignKey: 'user_id', as: 'owner' })
    }
  }
  Usergamebiodata.init({
    user_id: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    city: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Usergamebiodata',
  });
  return Usergamebiodata;
};