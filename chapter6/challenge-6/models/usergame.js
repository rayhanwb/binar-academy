'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Usergame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Usergame.hasOne(models.Usergamebiodata, { foreignKey: 'user_id', as: 'biodata' })
      Usergame.hasMany(models.Usergamehistory, { foreignKey: 'user_id', as: 'history' })
    }
  }
  Usergame.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Usergame',
  });
  return Usergame;
};