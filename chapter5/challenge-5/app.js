const PORT = 3000;
const express = require('express');
const app = express();

const morgan = require('morgan');
const router = require('./router');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.set('view engine', 'ejs');
app.use(express.json());
app.use(morgan('dev'));
app.use(router);

app.listen(PORT, () => console.log('listening on port', PORT));
