const express = require('express');
const router = express.Router();

const user = require('./controllers/user_gameController');
const biodata = require('./controllers/user_game_biodataController');
const history = require('./controllers/user_game_historyController');
const frontend = require('./controllers/frontendController');

router.get('/', frontend.home);

router.get('/users', user.getUsers);
router.get('/users/:id', user.getUser);
router.post('/users', user.createUser);
router.put('/users/:id', user.updateUser);
router.delete('/users/:id', user.deleteUser);

router.get('/biodata', biodata.getUserBiodatas);
router.get('/biodata/:id', biodata.getUserBiodata);
router.post('/biodata', biodata.createUserBiodata);
router.put('/biodata/:id', biodata.updateUserBiodata);
router.delete('/biodata/:id', biodata.deleteUserBiodata);

router.get('/history', history.getUserHistories);
router.get('/history/:id', history.getUserHistory);
router.post('/history', history.createUserHistory);
router.put('/history/:id', history.updateUserHistory);
router.delete('/history/:id', history.deleteUserHistory);
module.exports = router;
